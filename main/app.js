function addTokens(input, tokens){
    if(typeof input !== "string"){
        throw new TypeError("Invalid input")
    }else if(input.length<6){
        throw new TypeError("Input should have at least 6 characters")
    }
    for(let t in tokens){
        if(typeof tokens[t]["tokenName"] !=="string"){
            throw new TypeError("Invalid array format")
        }
    }
    if(input.indexOf('...')===-1){
        return input
    }

    //inlocuire
    let mod=input
    let x=new String("tokenName")
    for(let i=0;i<tokens.length;i++){
        let a=tokens[i]["tokenName"]
        let b="${"+a+"}"
        let c="..."
        let position=input.indexOf('...')
        if(position!==-1){
            mod=mod.replace(c,b)
        }
    }
    return mod
}

const app = {
    addTokens: addTokens
}

module.exports = app;